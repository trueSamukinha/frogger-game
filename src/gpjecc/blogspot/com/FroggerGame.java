
package gpjecc.blogspot.com;

import java.util.Iterator;
import java.util.Random;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

public class FroggerGame extends ApplicationAdapter {

	private SpriteBatch batch;
	private OrthographicCamera camera;
	private BitmapFont font;
	private InitScreen initScreen;
	private Array<Movel> veiculos;
	private Movel movel;
	private Movel sapo;
	private Frogg sapoGameWin;
	private Random rand;
	private Texture frogg;
	private Texture carBlue;
	private Texture carGreen;
	private Texture carYellow;
	private Texture truck;
	private Texture truckBombeiro;
	private Texture road;
	private Texture Wall;
	private Texture Finish;
	private Texture health;
	private Sound gameOverSound;
	private Sound victorySound;
	private Music backgroundMusic;

	public int fase = 1;
	public int jaSorteado = 6;
	public int auxiliarFecharTela = 1;
	public int tempo = 40;
	public boolean game2 = false;
	public boolean game2ChegouNoFinal = false;
	public boolean setMusicOneTime = false;
	public boolean gameWin1 = false;
	public boolean gameWin2 = false;
	private boolean gameOver = false;
	private boolean gameOver2 = false;
	private int seta1Vez = 0;
	private long lastDropTime;
	private int evitaSpawnMesmoLugar = 0;
	private long lastDropTime2;
	private int primeiraContagem = 0;

	private void carregaTexturas() {
		road = new Texture(Gdx.files.internal("assets/estrada.png"));
		Wall = new Texture(Gdx.files.internal("assets/muro.png"));
		Finish = new Texture(Gdx.files.internal("assets/linhaChegada.png"));
		frogg = new Texture(Gdx.files.internal("assets/sapo.png"));
		carBlue = new Texture(Gdx.files.internal("assets/CarroAzul.png"));
		carGreen = new Texture(Gdx.files.internal("assets/carroVerde.png"));
		carYellow = new Texture(Gdx.files.internal("assets/carroAmarelo.png"));
		truck = new Texture(Gdx.files.internal("assets/caminhao.png"));
		truckBombeiro = new Texture(Gdx.files.internal("assets/caminhaoDeBombeiro.png"));
		health = new Texture(Gdx.files.internal("assets/vida.png"));
	}

	private void carregaMusicas() {
		backgroundMusic = Gdx.audio.newMusic(Gdx.files.internal("assets/musicaFundo.mp3"));
		gameOverSound = Gdx.audio.newSound(Gdx.files.internal("assets/gameover.mp3"));
		victorySound = Gdx.audio.newSound(Gdx.files.internal("assets/vitoria.mp3"));
	}

	private void setaMusicaFundo(){
		backgroundMusic.setLooping(true);
		backgroundMusic.play();
	}

	private void carregaFontes() {
		font = new BitmapFont();
		font.setScale(2);
	}

	private void setaDimensaoTela(){
		camera = new OrthographicCamera();
		camera.setToOrtho(false, 800, 480);
		camera = new OrthographicCamera();
		camera.setToOrtho(false, 800, 480);
		batch = new SpriteBatch();

	}



	@Override
	public void create() {

		Texture.setEnforcePotImages(false);

		rand = new Random();

		carregaTexturas();
		carregaMusicas();

		setaMusicaFundo();

		setaDimensaoTela();
		batch = new SpriteBatch();
		carregaFontes();

		iniciaRodovia();

		spawnFrogg();

		veiculos = new Array<Movel>();

		spawnVehicles();

	}

	private void iniciaRodovia() {
		Rectangle rectangle = new Rectangle();
		rectangle.x = 0;
		rectangle.y = 70;
		rectangle.height = 225;
		rectangle.width = 800;
		initScreen = new Road(rectangle, road, null);
	}

	private void contagemTempo() {
		if (primeiraContagem == 0) {
			font.draw(batch, "TEMPO: " + tempo-- + " SEG", 270, 460);
			font.draw(batch, "TEMPO: " + tempo + " SEG", 270, 460);
			primeiraContagem++;
			lastDropTime2 = TimeUtils.nanoTime();
		}
		if (TimeUtils.nanoTime() - lastDropTime2 >= 1000000000) {
			font.setColor(Color.YELLOW);
			if (tempo > 0)
				font.draw(batch, "TEMPO: " + tempo-- + " SEG", 270, 460);
			lastDropTime2 = TimeUtils.nanoTime();
		}
		if (TimeUtils.nanoTime() - lastDropTime2 < 1000000000) {
			font.setColor(Color.YELLOW);
			font.draw(batch, "TEMPO: " + tempo + " SEG", 270, 460);
		}

	}

	private int sorteiaPosiçãoYDoVeiculo() {
		int numeros[] = new int[] { 80, 140, 250, 190 };

		int indice;
		Random random = new Random();
		indice = random.nextInt(numeros.length);

		while (evitaSpawnMesmoLugar == indice) {
			indice = random.nextInt(numeros.length);
		}
		evitaSpawnMesmoLugar = indice;
		return numeros[indice];
	}

	/**
	 * Cria os veiculos na tela
	 */
	private void spawnVehicles() {
		int sorteio = rand.nextInt(5);
		Rectangle rectangle = new Rectangle();
		rectangle.y = sorteiaPosiçãoYDoVeiculo();
		if (jaSorteado == sorteio) {
			spawnVehicles();
		}
		if (sorteio == 0) {
			rectangle.width = carBlue.getWidth() * 0.5f;
			rectangle.height = carBlue.getHeight() * 0.5f;
			movel = new Veiculos(rectangle, carBlue, null);
		} else if (sorteio == 1) {
			rectangle.width = carGreen.getWidth() * 0.5f;
			rectangle.height = carGreen.getHeight() * 0.5f;
			movel = new Veiculos(rectangle, carGreen, null);
		} else if (sorteio == 2) {
			rectangle.width = carYellow.getWidth() * 0.5f;
			rectangle.height = carYellow.getHeight() * 0.5f;
			movel = new Veiculos(rectangle, carYellow, null);
		} else if (sorteio == 3) {
			rectangle.width = truck.getWidth() * 0.5f;
			rectangle.height = truck.getHeight() * 0.5f;
			movel = new Veiculos(rectangle, truck, null);
		} else {
			rectangle.width = truckBombeiro.getWidth() * 0.5f;
			rectangle.height = truckBombeiro.getHeight() * 0.5f;
			movel = new Veiculos(rectangle, truckBombeiro, null);
		}
		jaSorteado = sorteio;
		rectangle.x = 0 - rectangle.width;
		veiculos.add(movel);
		lastDropTime = TimeUtils.nanoTime();
	}

	private void spawnFrogg() {
		Rectangle rectangle = new Rectangle();
		rectangle.x = 800 / 2 - 64 / 2;
		rectangle.y = 10;
		rectangle.width = frogg.getWidth();
		rectangle.height = frogg.getHeight();
		sapo = new Frogg(rectangle, frogg, null);
	}

	private void setMusic() {
		backgroundMusic.stop();
		if (gameOver && setMusicOneTime == false) {
			setMusicOneTime = true;
			gameOverSound.play();
		} else if (gameWin2 && setMusicOneTime == false) {
			setMusicOneTime = true;
			victorySound.play();
		}
	}

	private void carregaInitialElements() {
		if (fase == 1) {
			font.draw(batch, "FASE 1", 670, 460);
		} else if (fase == 2) {
			font.draw(batch, "FASE 2", 670, 460);
		}
		if (fase == 2 && seta1Vez == 0) {
			seta1Vez++;
			tempo = 40;
		}
		batch.draw(road, initScreen.getmRectangle().x, initScreen.getmRectangle().y, initScreen.getmRectangle().width,
				initScreen.getmRectangle().height);
		batch.draw(Wall, 0, 0, 800, 70);
		batch.draw(Finish, 0, 70 + initScreen.getmRectangle().height, 800, 50);
		batch.draw(sapo.getmImage(), sapo.getmRectangle().x, sapo.getmRectangle().y);
		for (Movel veiculo : veiculos) {
			batch.draw(veiculo.mImage, veiculo.getmRectangle().x, veiculo.getmRectangle().y,
					veiculo.getmRectangle().width, veiculo.getmRectangle().height);
		}
		font.setColor(Color.GREEN);
		font.draw(batch, String.valueOf(((Frogg) sapo).lives), 70, 460);
		batch.draw(health, 10, 460 - 40, 50, 50);
	}

	private void contagemTempoFase1() {
		if (!(gameOver) && !(gameWin1)) {
			contagemTempo();
		}
	}

	private void acabouOJogoOuPassouDeFase() {
		if (fase == 2 && !gameOver2 && seta1Vez == 1) {
			if (Gdx.input.isKeyPressed(Keys.ENTER)) {
				if (gameWin2 || tempo == 0) {
					Gdx.app.exit();
				} else {
					((Frogg) sapo).setPosition(800 / 2 - 64 / 2, 10);
					((Frogg) sapo).lives = 1;
					gameWin1 = false;
					game2 = true;
				}
			}
			if (game2 && seta1Vez == 1 && !gameWin2) {
				contagemTempo();
			}
			if (gameWin2) {
				tempo = 40;
			}
		}
	}

	private void verificaVenceuOuPerdeu() {
		if (gameWin1 || (!gameWin2 && (game2)) || gameWin2) {
			font.setColor(Color.CYAN);
			if (gameWin1)
				font.draw(batch, "VOCÊ VENCEU!", 800 / 2 - 150, 250);
			if (gameWin2) {
				setMusic();
				font.draw(batch, "VOCÊ VENCEU O JOGO!", 800 / 2 - 200, 250);
				font.draw(batch, "PRESSIONE ENTER PARA SAIR", 800 / 2 - 250, 200);
			} else {
				font.draw(batch, "PRESSIONE ENTER PARA CONTINUAR", 800 / 2 - 310, 200);
			}
			if (fase == 1 && gameWin1 == true) {
				fase = 2;
				gameWin2 = false;
				sapoGameWin.setgameWin2(gameWin2);
			} else if (fase == 2) {
				game2 = false;
				sapoGameWin.setgameWin2(gameWin2);
			}
		} else if (((Frogg) sapo).lives == 0 && auxiliarFecharTela == 1 || tempo == 0) {
			font.setColor(Color.RED);
			font.draw(batch, "VOCÊ PERDEU!", 800 / 2 - 150, 250);
			font.draw(batch, "PRESSIONE ENTER PARA FECHAR O PROGRAMA", 800 / 2 - 360, 200);
			gameOver = true;
			sapoGameWin.setgameOver(gameOver);
			setMusic();
			if (Gdx.input.isKeyPressed(Keys.ENTER))
				Gdx.app.exit();
		}
	}

	private void spawnVehiclesPeloTempo() {
		if (TimeUtils.nanoTime() - lastDropTime > 1000000000) {
			spawnVehicles();
		}
	}

	@Override
	public void render() {
		Gdx.gl.glClearColor(0, 0, 0.2f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		camera.update();

		batch.setProjectionMatrix(camera.combined);

		batch.begin();
		font.setColor(Color.YELLOW);

		carregaInitialElements();

		contagemTempoFase1();

		acabouOJogoOuPassouDeFase();

		sapoGameWin = (Frogg) sapo; // instacia um objeto da Classe Frogg

		verificaVenceuOuPerdeu();

		batch.end();

		sapo.handleEvent(camera);

		spawnVehiclesPeloTempo();

		Iterator<Movel> iter = veiculos.iterator();
		while (iter.hasNext()) {
			Movel vehicle = iter.next();
			vehicle.move();
			if (vehicle.getmRectangle().x > 800 + vehicle.getmRectangle().width) {
				iter.remove();
			}
			if (vehicle.getmRectangle().overlaps(sapo.getmRectangle())) {
				((Frogg) sapo).setPosition(800 / 2 - 64 / 2, 10);
				if (((Frogg) sapo).lives > 0 && (gameWin2 == false && !gameWin1) || game2 == true) {
					((Frogg) sapo).lives--;
					if (fase == 2) {
						gameOver2 = true;
					}
				}
			}
			if (sapo instanceof Frogg) {
				if (((Frogg) sapo).isOnFinishLine(sapo, initScreen, frogg, gameOver)) {
					if (fase == 1)
						gameWin1 = true;
					else if (fase == 2 && gameWin1 == false) {
						gameWin2 = true;
					}
				}
			}
		}

	}

	@Override
	public void dispose() {

		frogg.dispose();
		road.dispose();
		Wall.dispose();
		Finish.dispose();
		health.dispose();
		truckBombeiro.dispose();
		truck.dispose();
		carYellow.dispose();
		carGreen.dispose();
		carBlue.dispose();
		backgroundMusic.dispose();
		gameOverSound.dispose();
		victorySound.dispose();
		font.dispose();
		batch.dispose();
	}

}

abstract class Movel {

	protected Texture mImage;
	protected Rectangle mRectangle;
	protected Sound mSound;

	public Movel(Rectangle rectangle, Texture texture, Sound sound) {
		mRectangle = rectangle;
		mImage = texture;
		mSound = sound;
	}

	public abstract void move();

	public abstract void handleEvent(OrthographicCamera camera);

	public Texture getmImage() {
		return mImage;
	}

	public void setmImage(Texture mImage) {
		this.mImage = mImage;
	}

	public Rectangle getmRectangle() {
		return mRectangle;
	}

	public void setmRectangle(Rectangle mRectangle) {
		this.mRectangle = mRectangle;
	}

	public Sound getmSound() {
		return mSound;
	}

	public void setmSound(Sound mSound) {
		this.mSound = mSound;
	}
}

class InitScreen {

	protected Texture mImage;
	protected Rectangle mRectangle;
	protected Sound mSound;

	public InitScreen(Rectangle rectangle, Texture texture, Sound sound) {
		mRectangle = rectangle;
		mImage = texture;
		mSound = sound;
	}

	public Texture getmImage() {
		return mImage;
	}

	public void setmImage(Texture mImage) {
		this.mImage = mImage;
	}

	public Rectangle getmRectangle() {
		return mRectangle;
	}

	public void setmRectangle(Rectangle mRectangle) {
		this.mRectangle = mRectangle;
	}

}

class Road extends InitScreen {

	public Road(Rectangle rectangle, Texture texture, Sound sound) {
		super(rectangle, texture, sound);
	}
}

class Frogg extends Movel {

	public int lives = 3;
	private boolean gameover = false;
	private boolean gameWin2 = false;

	public Frogg(Rectangle rectangle, Texture texture, Sound sound) {
		super(rectangle, texture, sound);
	}

	private void movimentoSapo() {
		if (Gdx.input.isKeyPressed(Keys.LEFT) || Gdx.input.isKeyPressed(Keys.A)) {
			mRectangle.x -= 150 * Gdx.graphics.getDeltaTime();
			setTexture(new Texture(Gdx.files.internal("assets/sapoEsquerda.png")));
		} else if (Gdx.input.isKeyPressed(Keys.RIGHT) || Gdx.input.isKeyPressed(Keys.D)) {
			mRectangle.x += 150 * Gdx.graphics.getDeltaTime();
			setTexture(new Texture(Gdx.files.internal("assets/sapoDireita.png")));
		} else if (Gdx.input.isKeyPressed(Keys.DOWN) || Gdx.input.isKeyPressed(Keys.S)) {
			mRectangle.y -= 150 * Gdx.graphics.getDeltaTime();
			setTexture(new Texture(Gdx.files.internal("assets/sapoTras.png")));
		} else if (Gdx.input.isKeyPressed(Keys.UP) || Gdx.input.isKeyPressed(Keys.W)) {
			mRectangle.y += 150 * Gdx.graphics.getDeltaTime();
			setTexture(new Texture(Gdx.files.internal("assets/sapo.png")));
		}
	}

	private void limiteBordas() {
		if (mRectangle.x < 0)
			mRectangle.x = 0;
		if (mRectangle.x > 800 - 64)
			mRectangle.x = 800 - 64;
		if (mRectangle.y > 310)
			mRectangle.y = 310;
		if (mRectangle.y < 0)
			mRectangle.y = 0;
	}

	@Override
	public void handleEvent(OrthographicCamera camera) {
		if (!(getgameOver()) && !gameWin2) {
			movimentoSapo();
		}
		limiteBordas();
	}

	public void setgameOver(boolean gameOver) {
		this.gameover = gameOver;
	}

	private boolean getgameOver() {
		return gameover;
	}

	public void setgameWin2(boolean gameWin2) {
		this.gameWin2 = gameWin2;
	}

	public boolean isOnFinishLine(Movel sapo, InitScreen initScreen, Texture frogg, boolean gameOver) {
		if (sapo.getmRectangle().y > 55 + initScreen.getmRectangle().height + frogg.getHeight() && gameOver == false) {
			return true;
		} else {
			return false;
		}
	}

	private void setTexture(Texture texture) {
		setmImage(texture);

	}

	@Override
	public void move() {

	}

	public void setPosition(float x, float y) {
		mRectangle.x = x;
		mRectangle.y = y;
	}

}

class Veiculos extends Movel {

	public Veiculos(Rectangle rectangle, Texture texture, Sound sound) {
		super(rectangle, texture, sound);
	}

	@Override
	public void move() {
		getmRectangle().x += 200 * Gdx.graphics.getDeltaTime();
	}

	@Override
	public void handleEvent(OrthographicCamera camera) {

	}
}
